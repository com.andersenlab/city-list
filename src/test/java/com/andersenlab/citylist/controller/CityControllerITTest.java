package com.andersenlab.citylist.controller;

import com.andersenlab.citylist.util.TestPostgresContainer;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("tc")
public class CityControllerITTest {

    private static final TestPostgresContainer postgresContainer = TestPostgresContainer.getInstance();

    static {
        postgresContainer.start();
    }

    @Autowired
    private MockMvc mvc;

    @Nested
    public class GetCities {

        @Nested
        public class PaginatedSized {

            @Test
            public void getCitiesDefault_whenPerPageNegativeValue_thenShowAllPossibleList() throws Exception {
                mvc.perform(MockMvcRequestBuilders
                                .get("/api/cities?size=-1")
                                .accept(MediaType.APPLICATION_JSON))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(10))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].id").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].id").isNotEmpty())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].name").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].name").isNotEmpty())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].photo").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalElements").value(1000))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.size").value(10))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.number").value(0))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages").value(100))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.first").value(true))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.last").value(false));
            }

            @Test
            public void getCitiesDefault_whenPageOutOfTheMaxBound_thenReturnEmptyContentArray() throws Exception {
                mvc.perform(MockMvcRequestBuilders
                                .get("/api/cities?page=101")
                                .accept(MediaType.APPLICATION_JSON))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(0))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalElements").value(1000))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.size").value(10))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.number").value(101))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages").value(100))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.first").value(false))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.last").value(true));
            }

            @Test
            public void getCitiesDefault_whenPageLessThen1_thenReturnDefaultFirstPage() throws Exception {
                mvc.perform(MockMvcRequestBuilders
                                .get("/api/cities?page=-1")
                                .accept(MediaType.APPLICATION_JSON))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(10))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalElements").value(1000))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.size").value(10))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.number").value(0))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages").value(100))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.first").value(true))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.last").value(false));
            }

            @Test
            public void getCitiesDefault_whenPageSizeAndNumber_thenReturnPage5With50Elements() throws Exception {
                mvc.perform(MockMvcRequestBuilders
                                .get("/api/cities?page=5&size=50")
                                .accept(MediaType.APPLICATION_JSON))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(50))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalElements").value(1000))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.size").value(50))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.number").value(5))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages").value(20))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.first").value(false))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.last").value(false));
            }
        }

        @Nested
        public class SearchByName {

            @Test
            public void searchByCityName_whenPartOfName_thenReturnAllCitiesContainingName() throws Exception {
                mvc.perform(MockMvcRequestBuilders
                                .get("/api/cities?name=sUv")
                                .accept(MediaType.APPLICATION_JSON))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(1))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].id").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].id", Matchers.containsInRelativeOrder(634)))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].name").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].name", Matchers.containsInRelativeOrder("Suva")))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].photo").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].photo", Matchers.containsInRelativeOrder("https://upload.wikimedia.org/wikipedia/commons/e/e1/Fiji_map.png")))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalElements").value(1))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.size").value(10))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.number").value(0))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages").value(1))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.first").value(true))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.last").value(true));
            }

            @Test
            public void searchByCityName_whenNameIsSpace_thenReturnCitiesWithSpace() throws Exception {
                mvc.perform(MockMvcRequestBuilders
                                .get("/api/cities?name= ")
                                .accept(MediaType.APPLICATION_JSON))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content.size()").value(10))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].id").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].name").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.content[*].photo").exists())
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalElements").value(92))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.size").value(10))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.number").value(0))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages").value(10))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.first").value(true))
                        .andExpect(MockMvcResultMatchers.jsonPath("$.last").value(false));
            }

        }
    }

    @Nested
    public class UpdateCity {

        @Test
        public void updateCity_whenBlankNameIncorrectPhotoFormat_thenListOfExceptions() throws Exception {
            mvc.perform(MockMvcRequestBuilders
                            .patch("/api/cities/904")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\"name\": \"\", \"photo\":\"h\"}")
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isBadRequest())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations.size()").value(2))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[*].field", Matchers.containsInAnyOrder("photo", "name")))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Constraint Violation"));
        }
    }

    @Nested
    public class CreateCityTest {

        @Test
        public void updateCity_whenBlankNameIncorrectPhotoFormat_thenListOfExceptions() throws Exception {
            mvc.perform(MockMvcRequestBuilders
                            .post("/api/cities")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\"name\": \"\", \"photo\":\"h\"}")
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isBadRequest())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(400))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations.size()").value(2))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[*].field", Matchers.containsInAnyOrder("photo", "name")))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Constraint Violation"));
        }

        @Test
        public void updateCity_Test() throws Exception {
            mvc.perform(MockMvcRequestBuilders
                            .post("/api/cities")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{\"name\": \"newName\", \"photo\":\"http://\"}")
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isAccepted())
                    .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("newName"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.photo").value("http://"))
//                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations").exists())
//                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations.size()").value(2))
//                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[*].field", Matchers.containsInAnyOrder("photo", "name")))
//                    .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Constraint Violation"))
            ;
        }
    }
}
