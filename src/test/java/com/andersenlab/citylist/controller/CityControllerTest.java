package com.andersenlab.citylist.controller;

import com.andersenlab.citylist.dto.CityUpdateDto;
import com.andersenlab.citylist.model.City;
import com.andersenlab.citylist.service.CityService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

@WebMvcTest(CityController.class)
class CityControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CityService cityService;

    @Captor
    private ArgumentCaptor<Pageable> pageableArgument;
    @Captor
    private ArgumentCaptor<String> nameArgument;

    private City city;

    @BeforeEach
    public void init() {
        city = new City()
                .setId(1L)
                .setName("name")
                .setPhoto("photo");
        Page<City> page = new PageImpl<>(List.of(city));
        Mockito.doReturn(page)
                .when(cityService)
                .findAll(ArgumentMatchers.any(Pageable.class), ArgumentMatchers.any());
        Mockito.doReturn(city)
                .when(cityService)
                .update(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
        Mockito.doReturn(city)
                .when(cityService)
                .create(ArgumentMatchers.any());
    }

    @Nested
    public class GetCities {

        private void performGetEndpoint(String endPoint) throws Exception {
            mvc.perform(MockMvcRequestBuilders
                            .get(endPoint)
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isOk())
                    .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.content").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.totalElements").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.number").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.sort").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.size").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.totalPages").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.first").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.last").exists());
        }

        @Nested
        public class PaginatedSearch {

            @ParameterizedTest
            @ValueSource(strings = {"", "?size=-1", "?size=t", "?page=-1", "?page=T"})
            public void getCitiesDefault_whenPaginationParameter_thenDefaultPaginationSearchAndEndpointReturnValue(String paginationSettings) throws Exception {
                performGetEndpoint("/api/cities" + paginationSettings);

                Mockito.verify(cityService).findAll(pageableArgument.capture(), ArgumentMatchers.isNull());
                Assertions.assertThat(pageableArgument.getValue()).isNotNull();
                Assertions.assertThat(pageableArgument.getValue().getPageNumber()).isEqualTo(0);
                Assertions.assertThat(pageableArgument.getValue().getPageSize()).isEqualTo(10);
                Assertions.assertThat(pageableArgument.getValue().getSort()).isEqualTo(Sort.unsorted());
            }

            @Test
            public void getCitiesDefault_whenPage2_thenSecondPageAndEndpointReturnValue() throws Exception {
                performGetEndpoint("/api/cities?page=1");

                Mockito.verify(cityService).findAll(pageableArgument.capture(), ArgumentMatchers.isNull());
                Assertions.assertThat(pageableArgument.getValue()).isNotNull();
                Assertions.assertThat(pageableArgument.getValue().getPageNumber()).isEqualTo(1);
                Assertions.assertThat(pageableArgument.getValue().getPageSize()).isEqualTo(10);
                Assertions.assertThat(pageableArgument.getValue().getSort()).isEqualTo(Sort.unsorted());
            }

            @Test
            public void getCitiesDefault_when5PerPage_thenPageSize5AndEndpointReturnValue() throws Exception {
                performGetEndpoint("/api/cities?size=5");

                Mockito.verify(cityService).findAll(pageableArgument.capture(), ArgumentMatchers.isNull());
                Assertions.assertThat(pageableArgument.getValue()).isNotNull();
                Assertions.assertThat(pageableArgument.getValue().getPageNumber()).isEqualTo(0);
                Assertions.assertThat(pageableArgument.getValue().getPageSize()).isEqualTo(5);
                Assertions.assertThat(pageableArgument.getValue().getSort()).isEqualTo(Sort.unsorted());
            }

            @Test
            public void getCitiesDefault_whenPageSizeAndNumber_thenPageSizeAndNumberAndEndpointReturnValue() throws Exception {
                performGetEndpoint("/api/cities?page=5&size=50");

                Mockito.verify(cityService).findAll(pageableArgument.capture(), ArgumentMatchers.isNull());
                Assertions.assertThat(pageableArgument.getValue()).isNotNull();
                Assertions.assertThat(pageableArgument.getValue().getPageNumber()).isEqualTo(5);
                Assertions.assertThat(pageableArgument.getValue().getPageSize()).isEqualTo(50);
                Assertions.assertThat(pageableArgument.getValue().getSort()).isEqualTo(Sort.unsorted());
            }
        }

        @Nested
        public class SearchByName {

            @ParameterizedTest
            @ValueSource(strings = {"suva", "sUVa", "", " "})
            public void searchByCityName_whenSearchName_thenNamePassToServiceAndReturnAllCitiesContainingName(String searchName) throws Exception {
                performGetEndpoint("/api/cities?name=" + searchName);

                Mockito.verify(cityService).findAll(pageableArgument.capture(), nameArgument.capture());
                Assertions.assertThat(pageableArgument.getValue()).isNotNull();
                Assertions.assertThat(pageableArgument.getValue().getPageNumber()).isEqualTo(0);
                Assertions.assertThat(pageableArgument.getValue().getPageSize()).isEqualTo(10);
                Assertions.assertThat(pageableArgument.getValue().getSort()).isEqualTo(Sort.unsorted());
                Assertions.assertThat(nameArgument.getValue()).isNotNull();
                Assertions.assertThat(nameArgument.getValue()).isEqualTo(searchName);
            }

            @Test
            public void searchByCityName_whenSearchNameAndPageAndSize_thenNameSizePagePassToServiceAndReturnAllCitiesContainingName() throws Exception {
                performGetEndpoint("/api/cities?name=n&page=5&size=20");

                Mockito.verify(cityService).findAll(pageableArgument.capture(), nameArgument.capture());
                Assertions.assertThat(pageableArgument.getValue()).isNotNull();
                Assertions.assertThat(pageableArgument.getValue().getPageNumber()).isEqualTo(5);
                Assertions.assertThat(pageableArgument.getValue().getPageSize()).isEqualTo(20);
                Assertions.assertThat(pageableArgument.getValue().getSort()).isEqualTo(Sort.unsorted());
                Assertions.assertThat(nameArgument.getValue()).isNotNull();
                Assertions.assertThat(nameArgument.getValue()).isEqualTo("n");
            }
        }
    }

    @Nested
    public class UpdateCity {

        @ParameterizedTest
        @CsvSource({
                "1, 0",
                "255, 992"
        })
        public void updateCity_whenCorrectIdNamePhoto_thenReceiveUpdatedCity(int nameNumberOfSymbols, int photoNumberOfExtraSymbols) throws Exception {
            mvc.perform(MockMvcRequestBuilders
                            .patch("/api/cities/1")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(asJsonString(new CityUpdateDto(
                                    "A".repeat(nameNumberOfSymbols),
                                    "https://" + " ".repeat(photoNumberOfExtraSymbols)
                            )))
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isAccepted())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.name").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.photo").exists());

            Mockito.verify(cityService).update(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
        }

        @ParameterizedTest
        @ValueSource(ints = {993, 1000})
        public void updateCity_whenTooLongPhoto_thenException(int numberOfExtraSymbols) throws Exception {
            mvc.perform(MockMvcRequestBuilders
                            .patch("/api/cities/1")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(asJsonString(new CityUpdateDto("Anyang4", "https://" + " ".repeat(numberOfExtraSymbols))))
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isBadRequest())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("BAD_REQUEST"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations.size()").value(1))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[0].field").value("photo"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[0].message").value("size must be between 0 and 1000"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Constraint Violation"));

            Mockito.verify(cityService, Mockito.never()).update(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
        }

        @ParameterizedTest
        @ValueSource(ints = {256, 300})
        public void updateCity_whenTooLongName_thenException(int numberOfSymbols) throws Exception {
            mvc.perform(MockMvcRequestBuilders
                            .patch("/api/cities/1")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(asJsonString(new CityUpdateDto("A".repeat(numberOfSymbols), "http://")))
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isBadRequest())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("BAD_REQUEST"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations.size()").value(1))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[0].field").value("name"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[0].message").value("size must be between 0 and 255"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Constraint Violation"));

            Mockito.verify(cityService, Mockito.never()).update(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
        }

        @ParameterizedTest
        @ValueSource(ints = {0, 1, 10})
        public void updateCity_whenBlankName_thenException(int numberOfSymbols) throws Exception {
            mvc.perform(MockMvcRequestBuilders
                            .patch("/api/cities/1")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(asJsonString(new CityUpdateDto(" ".repeat(numberOfSymbols), "http://")))
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isBadRequest())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("BAD_REQUEST"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations.size()").value(1))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[0].field").value("name"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[0].message").value("must not be blank"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Constraint Violation"));

            Mockito.verify(cityService, Mockito.never()).update(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
        }

        @Test
        public void updateCity_whenNullPhoto_thenReceiveUpdatedCity() throws Exception {
            city.setPhoto(null);

            mvc.perform(MockMvcRequestBuilders
                            .patch("/api/cities/1")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(asJsonString(new CityUpdateDto(city.getName(), city.getPhoto())))
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isAccepted())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.name").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("name"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.photo").doesNotExist());

            Mockito.verify(cityService).update(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
        }

        @Test
        public void updateCity_whenEmptyPhoto_thenReceiveUpdatedCity() throws Exception {
            city.setPhoto("");

            mvc.perform(MockMvcRequestBuilders
                            .patch("/api/cities/1")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(asJsonString(new CityUpdateDto(city.getName(), city.getPhoto())))
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isAccepted())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.name").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("name"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.photo").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.photo").value(""));

            Mockito.verify(cityService).update(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
        }

        @Test
        public void updateCity_whenIncorrectPhotoFormat_thenException() throws Exception {
            mvc.perform(MockMvcRequestBuilders
                            .patch("/api/cities/1")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(asJsonString(new CityUpdateDto("Anyang4", "h")))
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isBadRequest())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("BAD_REQUEST"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations.size()").value(1))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[0].field").value("photo"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[0].message", Matchers.containsString("must match")))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Constraint Violation"));

            Mockito.verify(cityService, Mockito.never()).update(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
        }

        @Test
        public void updateCity_whenBlankNameIncorrectPhotoFormat_thenListOfExceptions() throws Exception {
            mvc.perform(MockMvcRequestBuilders
                            .patch("/api/cities/1")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(asJsonString(new CityUpdateDto("", "h")))
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isBadRequest())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("BAD_REQUEST"))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations.size()").value(2))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.violations[*].field", Matchers.containsInAnyOrder("photo", "name")))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Constraint Violation"));

            Mockito.verify(cityService, Mockito.never()).update(ArgumentMatchers.anyLong(), ArgumentMatchers.any());
        }
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Nested
    public class CreateCityTest {

        @ParameterizedTest
        @CsvSource({
                "1, 0",
                "255, 992"
        })
        public void updateCity_whenCorrectIdNamePhoto_thenReceiveUpdatedCity(int nameNumberOfSymbols, int photoNumberOfExtraSymbols) throws Exception {
            mvc.perform(MockMvcRequestBuilders
                            .post("/api/cities")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(asJsonString(new CityUpdateDto(
                                    "A".repeat(nameNumberOfSymbols),
                                    "https://" + " ".repeat(photoNumberOfExtraSymbols)
                            )))
                            .accept(MediaType.APPLICATION_JSON))
                    .andExpect(MockMvcResultMatchers.status().isAccepted())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                    .andExpect(MockMvcResultMatchers.jsonPath("$.name").exists())
                    .andExpect(MockMvcResultMatchers.jsonPath("$.photo").exists());

            Mockito.verify(cityService).create(ArgumentMatchers.any());
        }
    }
}