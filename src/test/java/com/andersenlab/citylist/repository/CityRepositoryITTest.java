package com.andersenlab.citylist.repository;

import com.andersenlab.citylist.model.City;
import com.andersenlab.citylist.util.TestPostgresContainer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
@ActiveProfiles("tc")
class CityRepositoryITTest {

    private static final TestPostgresContainer postgresContainer = TestPostgresContainer.getInstance();

    static {
        postgresContainer.start();
    }

    @Autowired
    private CityRepository cityRepository;

    @Test
    public void givenDefaultCitiesDB_whenFindAllPaginated_thenReceive() {
        Page<City> cities = cityRepository.findAll((root, query, builder) -> builder.conjunction(), Pageable.ofSize(5));

        Assertions.assertThat(cities).isNotNull();
        Assertions.assertThat(cities.getSize()).isEqualTo(5);
        Assertions.assertThat(cities.getContent()).isNotNull();
        Assertions.assertThat(cities.getContent().size()).isEqualTo(5);
        Assertions.assertThat(cities.getNumber()).isEqualTo(0);
    }

    @Test
    public void givenInitialCity_whenSaveChanges_thenEntityUpdated() {
        var initialCity = cityRepository.findById(904L).orElseThrow();
        Assertions.assertThat(initialCity).isNotNull();
        Assertions.assertThat(initialCity.getId()).isEqualTo(904L);
        Assertions.assertThat(initialCity.getName()).isEqualTo("Anyang");
        Assertions.assertThat(initialCity.getPhoto()).isEqualTo("https://upload.wikimedia.org/wikipedia/commons/e/ee/Yinxu.jpg");

        var cityToSave = new City().setId(904L).setName("Anyang1").setPhoto("photo");

        City resultToTest = cityRepository.save(cityToSave);
        cityRepository.flush();

        Assertions.assertThat(resultToTest).isNotNull();
        Assertions.assertThat(resultToTest.getId()).isEqualTo(904L);
        Assertions.assertThat(resultToTest.getName()).isEqualTo("Anyang1");
        Assertions.assertThat(resultToTest.getPhoto()).isEqualTo("photo");
    }

    @Test
    public void givenDefaultCitiesDB_whenSaveNullPhoto_thenEntityUpdated() {
        var initialCity = cityRepository.findById(904L).orElseThrow();
        Assertions.assertThat(initialCity).isNotNull();
        Assertions.assertThat(initialCity.getId()).isEqualTo(904L);
        Assertions.assertThat(initialCity.getName()).isEqualTo("Anyang");
        Assertions.assertThat(initialCity.getPhoto()).isEqualTo("https://upload.wikimedia.org/wikipedia/commons/e/ee/Yinxu.jpg");

        var cityToSave = new City().setId(904L).setName("Anyang1").setPhoto(null);

        City resultToTest = cityRepository.save(cityToSave);
        cityRepository.flush();

        Assertions.assertThat(resultToTest).isNotNull();
        Assertions.assertThat(resultToTest.getId()).isEqualTo(904L);
        Assertions.assertThat(resultToTest.getName()).isEqualTo("Anyang1");
        Assertions.assertThat(resultToTest.getPhoto()).isNull();
    }

    @Test
    public void givenDefaultCitiesDB_whenSaveNullName_thenException() {
        var initialCity = cityRepository.findById(904L).orElseThrow();
        Assertions.assertThat(initialCity).isNotNull();
        Assertions.assertThat(initialCity.getId()).isEqualTo(904L);
        Assertions.assertThat(initialCity.getName()).isEqualTo("Anyang");
        Assertions.assertThat(initialCity.getPhoto()).isEqualTo("https://upload.wikimedia.org/wikipedia/commons/e/ee/Yinxu.jpg");

        var cityToSave = new City().setId(904L).setName(null);

        cityRepository.save(cityToSave);
        var exception = assertThrows(DataIntegrityViolationException.class, () -> cityRepository.flush());

        Assertions.assertThat(exception.getMessage()).contains("constraint [name\" of relation \"city]");
    }
}