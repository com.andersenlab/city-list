package com.andersenlab.citylist.mapper;

import com.andersenlab.citylist.dto.CityUpdateDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

class CityMapperTest {

    private final CityMapper mapper = Mappers.getMapper(CityMapper.class);

    @Test
    public void mapperWorksTest() {
        CityUpdateDto dto = new CityUpdateDto("name", "http");

        var resultToTest = mapper.convert(dto);

        Assertions.assertNotNull(resultToTest);
        Assertions.assertNull(resultToTest.getId());
        Assertions.assertEquals("name", resultToTest.getName());
        Assertions.assertEquals("http", resultToTest.getPhoto());
    }

    @Test
    public void mapperWorksWithNullsTest() {
        CityUpdateDto dto = new CityUpdateDto(null, null);

        var resultToTest = mapper.convert(dto);

        Assertions.assertNotNull(resultToTest);
        Assertions.assertNull(resultToTest.getId());
        Assertions.assertNull(resultToTest.getName());
        Assertions.assertNull(resultToTest.getPhoto());
    }
}