package com.andersenlab.citylist.service;

import com.andersenlab.citylist.dto.CityUpdateDto;
import com.andersenlab.citylist.mapper.CityMapper;
import com.andersenlab.citylist.model.City;
import com.andersenlab.citylist.repository.CityRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.zalando.problem.DefaultProblem;

@ExtendWith(MockitoExtension.class)
public class CityServiceTest {

    @InjectMocks
    private CityService cityService;

    @Mock
    private CityRepository cityRepository;
    @Mock
    private CityMapper cityMapper;

    @Nested
    public class FindAll {

        @Captor
        private ArgumentCaptor<Specification<City>> specificationCaptor;

        @ParameterizedTest
        @CsvSource({
                "0, 1, searchName",
                "4, 5,",
        })
        public void findAll_thenExecuteRepositoryMethod(int page, int size, String name) {
            Pageable pageable = PageRequest.of(page, size, Sort.unsorted());
            ArgumentCaptor<Pageable> pageableArgument = ArgumentCaptor.forClass(Pageable.class);

            cityService.findAll(pageable, name);

            Mockito.verify(cityRepository).findAll(specificationCaptor.capture(), pageableArgument.capture());
            Assertions.assertThat(pageableArgument.getValue()).isNotNull();
            Assertions.assertThat(pageableArgument.getValue().getPageNumber()).isEqualTo(page);
            Assertions.assertThat(pageableArgument.getValue().getPageSize()).isEqualTo(size);
            Assertions.assertThat(pageableArgument.getValue().getSort()).isEqualTo(Sort.unsorted());
            Assertions.assertThat(specificationCaptor.getValue()).isNotNull();
        }
    }

    @Nested
    public class Update {

        @Test
        public void updateCity_whenPassData_thenDataIsSaved() {
            Mockito.doReturn(true)
                    .when(cityRepository)
                    .existsById(ArgumentMatchers.anyLong());
            Mockito.when(cityRepository.save(ArgumentMatchers.any(City.class)))
                    .thenAnswer(
                            answer -> answer.getArgument(0, City.class));
            Mockito.when(cityMapper.convert(ArgumentMatchers.any(CityUpdateDto.class)))
                    .thenAnswer(
                            answer -> new City()
                                    .setName(answer.getArgument(0, CityUpdateDto.class).name())
                                    .setPhoto(answer.getArgument(0, CityUpdateDto.class).photo())
                    );
            var dto = new CityUpdateDto("name", "photo");

            var resultToTest = cityService.update(1L, dto);

            Assertions.assertThat(resultToTest).isNotNull();
            Assertions.assertThat(resultToTest.getId()).isEqualTo(1L);
            Assertions.assertThat(resultToTest.getName()).isEqualTo("name");
            Assertions.assertThat(resultToTest.getPhoto()).isEqualTo("photo");
            Mockito.inOrder(cityRepository, cityMapper);
            Mockito.verify(cityRepository).existsById(1L);
            Mockito.verify(cityMapper).convert(ArgumentMatchers.any(CityUpdateDto.class));
            Mockito.verify(cityRepository).save(ArgumentMatchers.any(City.class));
        }

        @Test
        public void updateCity_whenNoCityWithId_thenException() {
            Mockito.doReturn(false)
                    .when(cityRepository)
                    .existsById(ArgumentMatchers.anyLong());

            Assertions.assertThatThrownBy(() -> cityService.update(1L, null))
                    .isInstanceOf(DefaultProblem.class)
                    .hasMessage("Not Found: No city found with provided ID");

            Mockito.verify(cityRepository).existsById(1L);
            Mockito.verify(cityMapper, Mockito.never()).convert(ArgumentMatchers.any(CityUpdateDto.class));
            Mockito.verify(cityRepository, Mockito.never()).save(ArgumentMatchers.any(City.class));
        }
    }

    @Nested
    public class CreateTest {

        @Test
        public void updateCity_whenPassData_thenDataIsSaved() {
            Mockito.when(cityRepository.save(ArgumentMatchers.any(City.class)))
                    .thenAnswer(
                            answer -> answer.getArgument(0, City.class).setId(1L));
            Mockito.when(cityMapper.convert(ArgumentMatchers.any(CityUpdateDto.class)))
                    .thenAnswer(
                            answer -> new City()
                                    .setName(answer.getArgument(0, CityUpdateDto.class).name())
                                    .setPhoto(answer.getArgument(0, CityUpdateDto.class).photo())
                    );
            var dto = new CityUpdateDto("name", "photo");

            var resultToTest = cityService.create(dto);

            Assertions.assertThat(resultToTest).isNotNull();
            Assertions.assertThat(resultToTest.getId()).isEqualTo(1L);
            Assertions.assertThat(resultToTest.getName()).isEqualTo("name");
            Assertions.assertThat(resultToTest.getPhoto()).isEqualTo("photo");
            Mockito.inOrder(cityRepository, cityMapper);
            Mockito.verify(cityMapper).convert(ArgumentMatchers.any(CityUpdateDto.class));
            Mockito.verify(cityRepository).save(ArgumentMatchers.any(City.class));
        }
    }
}