package com.andersenlab.citylist.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public record  CityUpdateDto(

        @NotBlank
        @Size(max = 255)
        String name,

        @Pattern(regexp = "^$|^(http|https|file)://.*")
        @Size(max = 1000)
        String photo
) {

}
