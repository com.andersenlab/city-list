package com.andersenlab.citylist.service;

import com.andersenlab.citylist.dto.CityUpdateDto;
import com.andersenlab.citylist.mapper.CityMapper;
import com.andersenlab.citylist.model.City;
import com.andersenlab.citylist.repository.CityRepository;
import com.andersenlab.citylist.specification.CitySpecification;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.ThrowableProblem;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CityService {

    CityRepository cityRepository;
    CityMapper cityMapper;

    public Page<City> findAll(Pageable pageable, String name) {
        return cityRepository.findAll(CitySpecification.getFilterCityByName(name), pageable);
    }

    public City update(long cityId, CityUpdateDto city) {
        if (!cityRepository.existsById(cityId)) {
            throw notFound(cityId);
        }
        return cityRepository.save(cityMapper.convert(city).setId(cityId));
    }

    public City create(CityUpdateDto city) {
        return cityRepository.save(cityMapper.convert(city));
    }

    private ThrowableProblem notFound(long cityId) {
        return Problem.builder()
                .withStatus(Status.NOT_FOUND)
                .withTitle(Status.NOT_FOUND.getReasonPhrase())
                .withDetail("No city found with provided ID")
                .with("cityId", cityId)
                .build();
    }
}
