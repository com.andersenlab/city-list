package com.andersenlab.citylist.controller;

import com.andersenlab.citylist.dto.CityUpdateDto;
import com.andersenlab.citylist.model.City;
import com.andersenlab.citylist.service.CityService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("api/cities")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CityController {

    CityService cityService;

    @GetMapping
    public Page<City> findAll(@ParameterObject @PageableDefault Pageable pageable, @Parameter @Nullable String name) {
        return cityService.findAll(pageable, name);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PatchMapping("{id}")
    public City update(@PathVariable long id, @RequestBody @Valid CityUpdateDto city) {
        return cityService.update(id, city);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PostMapping
    public City create(@RequestBody @Valid CityUpdateDto city) {
        return cityService.create(city);
    }
}
