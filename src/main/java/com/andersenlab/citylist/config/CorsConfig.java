package com.andersenlab.citylist.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableConfigurationProperties({CorsSettings.class})
@RequiredArgsConstructor
public class CorsConfig implements WebMvcConfigurer {

    private final CorsSettings corsSettings;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        var corsConfiguration = corsSettings.getCors();
        registry.addMapping("/**")
                .combine(corsConfiguration);
    }
}
