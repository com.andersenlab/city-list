package com.andersenlab.citylist.mapper;

import com.andersenlab.citylist.dto.CityUpdateDto;
import com.andersenlab.citylist.model.City;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface CityMapper {

    @Mapping(target = "id", ignore = true)
    City convert(CityUpdateDto dto);
}
