package com.andersenlab.citylist.specification;

import com.andersenlab.citylist.model.City;
import org.springframework.data.jpa.domain.Specification;

public class CitySpecification {

    public static Specification<City> getFilterCityByName(String name) {
        return (root, query, criteriaBuilder) ->
                name == null
                        ? criteriaBuilder.conjunction()
                        : criteriaBuilder.and(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),
                        "%" + name.toLowerCase() + "%"));
    }
}
