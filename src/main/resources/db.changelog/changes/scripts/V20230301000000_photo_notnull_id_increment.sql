ALTER TABLE city
    ALTER COLUMN photo DROP NOT NULL;

CREATE SEQUENCE city_id_seq RESTART 1001;

ALTER TABLE city
    ALTER COLUMN id SET DEFAULT nextval('city_id_seq'::regclass);