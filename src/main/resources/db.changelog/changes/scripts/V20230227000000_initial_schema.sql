CREATE TABLE city (
                             id int8 NOT NULL,
                             name varchar(255) not null,
                             photo varchar(1000) not null,
                             CONSTRAINT city_pkey PRIMARY KEY (id)
);

CREATE INDEX idx_city_name ON city USING btree (name);
