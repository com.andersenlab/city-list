# City-list
## Application to display list of cities (name and photo)

### Application features:

* browse through the paginated list of cities with the corresponding photos
* search by the name
* edit the city (both name and photo)

### Info:
* initial list of cities should be populated using the attached cities.csv file
* city addition, deletion and sorting are not in the scope of this task

### Technical requirements
* Spring Boot
* Any build system
* Any database
* Any frontend stack
* Spring Data REST is prohibited

### Documentation

* [Swagger UI](http://localhost:8080/swagger-ui/index.html)
* [API Docs in JSON format](http://localhost:8080/v3/api-docs)

### Instruction to run
You can run application from your IDE or start docker container and do work on frontend code

1. First of all in any case application need database to operate. 
Create file `.env` in `docker` folder. As login and password are sensitive data then this file is in `.gitignore` list.
This is example of data to store
    ```
    DATASOURCE_URL=jdbc:postgresql://host.docker.internal:5432/city_list
    POSTGRES_DB=city_list
    POSTGRES_USER=city_user
    POSTGRES_PASSWORD=city_db_pass
    ```

2. For development of backend we can start database only container using
docker compose file `docker/docker-compose.db.yml`. 
If backend is ready and you need application up and running to develop frontend or 
to show application work just go to point 4 of this instruction
   ```sh
   docker compose -f ./docker/docker-compose.db.yml up -d
   ```

3. For running with Intellij IDEA you need edit configuration of running Spring Boot 
and set "Environment variables" (Modify options-Operating system-Environment variables)
Example:
   ```
   DATASOURCE_URL=jdbc:postgresql:///city_list;POSTGRES_USER=city_user;POSTGRES_PASSWORD=city_db_pass
   ```
   After that you can run application


4. When you finished with backend and just need running application to continue with frontend 
it is possible to run database and application in containers without need to use memory on IDE 
   * on Lunix base OS
    ```sh
    ./gradlew clean build
    ```
   * on Windows
    ```shell
    gradlew.bat clean build
    ```
   This will create necessary jar file of application.
   
   Run command to create and run containers with DB and application
   ```sh
   docker compose -f ./docker/docker-compose.yml up -d
   ```
